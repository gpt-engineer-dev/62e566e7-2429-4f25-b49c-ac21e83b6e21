// 1. Initialize the game state
const gameState = {
  board: ['', '', '', '', '', '', '', '', ''],
  currentPlayer: 'X',
  winner: null
};

// 2. Handle click events on each cell
document.querySelectorAll('#game-board button').forEach((cell, index) => {
  cell.addEventListener('click', () => makeMove(index));
});

// 3. Check for a win or a tie
function checkWin() {
  // Define winning combinations
  const winCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  // Check for win
  for (const combination of winCombinations) {
    const [a, b, c] = combination;
    if (gameState.board[a] && gameState.board[a] === gameState.board[b] && gameState.board[a] === gameState.board[c]) {
      gameState.winner = gameState.currentPlayer;
      updateStatus(`Player ${gameState.winner} wins!`);
      highlightWin(combination);
      return;
    }
  }

  // Check for tie
  if (!gameState.board.includes('')) {
    gameState.winner = 'Tie';
    updateStatus("It's a tie!");
  }
}

// 4. Toggle the player's turn
function togglePlayer() {
  gameState.currentPlayer = gameState.currentPlayer === 'X' ? 'O' : 'X';
  updateStatus(`Player ${gameState.currentPlayer}'s turn`);
}

// 5. Implement a reset function
document.getElementById('reset-button').addEventListener('click', resetGame);

function resetGame() {
  gameState.board.fill('');
  gameState.currentPlayer = 'X';
  gameState.winner = null;
  updateStatus(`Player ${gameState.currentPlayer}'s turn`);
  document.querySelectorAll('#game-board button').forEach(cell => {
    cell.textContent = '';
    cell.classList.remove('winning-cell');
  });
}

// 6. Update the UI
function makeMove(index) {
  if (!gameState.board[index] && !gameState.winner) {
    gameState.board[index] = gameState.currentPlayer;
    document.querySelectorAll('#game-board button')[index].textContent = gameState.currentPlayer;
    checkWin();
    if (!gameState.winner) {
      togglePlayer();
    }
  }
}

function updateStatus(message) {
  document.getElementById('game-status').textContent = message;
}

function highlightWin(combination) {
  combination.forEach(index => {
    document.querySelectorAll('#game-board button')[index].classList.add('winning-cell');
  });
}

// Initialize the game
resetGame();
